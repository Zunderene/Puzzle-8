package Algoritmos;

import java.util.ArrayList;

import Pak.Nodo;

public  class BusquedaProfundidadIterativa extends AlgoritmoBusqueda{
	private String Nombre;
	private int Limite;
	
	public BusquedaProfundidadIterativa(Nodo ini, Nodo f, int limite){
		nodo=ini;
		resultado=ini;
		EstFinal=f;
		Nombre="Busqueda Profundidad Iterativa";
		Limite=limite;
                
	}
	
	public void Busqueda(){
		int prof =1;
		//Inicializacion//
				Abierto().add(nodo);
				resultado=Abierto().get(0);
			while(!esIgual(resultado,EstFinal) && prof<Limite){
				Abierto().add(nodo);
				resultado=Abierto().get(0);
				
				while(Abierto().size()>0 && !esIgual(resultado,EstFinal)){
					Cerrado().add(resultado);
					
					//Generacion de descendencia//
					
					if(prof>resultado.getProfundidad()){
						this.insertarHijos(resultado.generarHijo());
						this.repite(Hijos(), Abierto(), Cerrado());
						this.insertarAbierto();
					}
					this.Memoria();
					resultado=Abierto().get(Abierto().size()-1);
					Abierto().remove(Abierto().size()-1);
					Hijos().clear();
				}
				prof++;
				Abierto().clear();
				Cerrado().clear();
				Hijos().clear();
			}

		if(esIgual(resultado,EstFinal))
			encontrado=true;
		else
			encontrado=false;
}
	
public  void repite(ArrayList<Nodo> Hijos , ArrayList<Nodo> Abierto , ArrayList<Nodo> Cerrado ){
	int i=0,j=0,k=0;
	boolean encontrado=false;
	while(i<Hijos().size()){
		j=0;
		encontrado=false;
		while(j<Abierto().size() && !encontrado){
			if(esIgual(Hijos().get(i),Abierto().get(j))){
				if(Hijos().get(i).getProfundidad()< Abierto().get(j).getProfundidad())
					Abierto().set(j, Hijos().get(i));
				Hijos().remove(i);
				encontrado=true;
				i--;
			}
			/*else if(Hijos().get(i).getProfundidad()< Abierto().get(j).getProfundidad()){
				Abierto().remove(j); //Duda
				encontrado=true;
				i--;
			}*/
			j++;
		}
		k=0;
		while(k<Cerrado().size()&& !encontrado){
			if(esIgual(Hijos().get(i),Cerrado().get(k))){
				Hijos().remove(i);
				encontrado=true;
				i--;
			}
			k++;
		}
		i++;
		
	}
	
}
	
	public String GetNombre() {return Nombre;}
	
	
}
