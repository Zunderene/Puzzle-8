package Pak;

import java.util.ArrayList;

public class Nodo {
	private Nodo Padre;
	private int estado [][];
	private int vacio_x, vacio_y;
	private int Profundidad;
	private int coste;
	
	
	public Nodo(){        //Constructor Genera 123456780 Estado Final
		estado = new int [3][3];
		int a=1;
		for(int i=0 ; i<3 ; i++){
			for(int j=0; j<3; j++){
				estado[i][j]=a++;
			}
		}
		estado[2][2]=0;
		Padre=null;
		vacio_x=2;
		vacio_y=2;
		Profundidad =0;
	}
	
	public Nodo( Nodo P, int pos_x, int pos_y){ //Constructor generador de hijos
		estado = new int [3][3];
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				estado[i][j]=P.estado[i][j];
			}
		}
	
		estado[P.vacio_x][P.vacio_y]=P.estado[pos_x][pos_y];
		estado[pos_x][pos_y]=0;
		Padre=P;
		vacio_x=pos_x;
		vacio_y=pos_y;
		Profundidad=P.Profundidad+1;
	}
	
	
	
	public Nodo(int b[][]){  //Constructor que Crea un nodo con un estado dependiende de lo que pasamos por parametro
		estado = new int [3][3];
		
		for(int i=0 ; i<3 ; i++){
			for(int j=0; j<3; j++){
				estado[i][j]=b [i][j];
				if(b[i][j]==0){
					vacio_x=i;vacio_y=j;
				}
			}
		}
		Padre=null;
	}
	
	
	public ArrayList<Nodo> generarHijo (){      
		ArrayList<Nodo> Hijos = new ArrayList<Nodo>();
		Nodo hijo;
		
		for(int i=0; i<4; i++){
			if(i==0 && vacio_x>0){
				hijo=new Nodo(this,vacio_x-1,vacio_y);
				Hijos.add(hijo);
			}
			
			else if(i==1 && vacio_x<2){
				hijo=new Nodo(this,vacio_x+1,vacio_y);
				Hijos.add(hijo);
			}
			
			else if(i==2 && vacio_y>0){
				hijo=new Nodo(this,vacio_x,vacio_y-1);
				Hijos.add(hijo);
			}
			
			else if(i==3 && vacio_y<2){
				hijo=new Nodo(this,vacio_x,vacio_y+1);
				Hijos.add(hijo);
			}
		
		}
		
		return Hijos;
	}
	 
	public int  getestados(int x, int y){return estado[x][y];}
	public int getProfundidad(){return Profundidad;}
	public Nodo getPadre(){return Padre;}
	public int getcoste(){return coste;}
	
	
	public String toString(int n){
		String aux;
		aux="\n";
		for(int i=0; i<3; i++){
			aux=aux+"\n";
			for(int j=0; j<3;j++){
				aux=aux+estado[i][j];
			}
		}
		return aux;
	}
	
	public String toString(){
		String aux;
		aux=" ";
		for(int i=0; i<3; i++){
			for(int j=0; j<3;j++){
				aux=aux+estado[i][j];
			}
		}
		return aux;
	}
	public void coste(Nodo ide){
		int cont=0;
		int cont1=0;
		int aux=0;
		int posx=0,posy=0;
		for(int j=0; j<3; j++){
			for(int k=0; k<3; k++){
				if(estado[j][k]!=ide.estado[j][k] && estado[j][k]!=0){
					cont++;
					}
				}
			}
		//Mahatan//
		for(int j=0; j<3; j++){
			for(int k=0; k<3; k++){
				if(estado[j][k]!=ide.estado[j][k] && estado[j][k]!=0){
					//Busqueda del ocho en el EstFinal//
					posy=BusquedaPosicional_x(estado[j][k],ide);
					posx=BusquedaPosicional_y(estado[j][k],ide);
					aux=Math.abs(j-posy);
					cont1=cont1+aux+Math.abs(k-posx);
				}
					
			}
		}
		coste=cont+cont1+this.Profundidad;
	}
	public int BusquedaPosicional_x(int num,Nodo EstFinal){
		int i=0;int j=0;
		boolean encontrado=false;
		while(i<3 && !encontrado){
			j=0;
			while(j<3 && !encontrado){
				if(EstFinal.estado[i][j]==num){
					encontrado=true;
				}
				j++;
				
			}
			i++;
		}
		return i-1;
	}
	public int BusquedaPosicional_y(int num,Nodo EstFinal){
		int i=0;int j=0;
		boolean encontrado=false;
		while(i<3 && !encontrado){
			j=0;
			while(j<3 && !encontrado){
				if(EstFinal.estado[i][j]==num){
					encontrado=true;
				}
				j++;
				
			}
			i++;
		}
		return j-1;
	}
		
}


